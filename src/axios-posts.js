import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://gearby-f701b.firebaseio.com/'
});

export default instance;