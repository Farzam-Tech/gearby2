import React, { Component } from 'react';
import axios from '../../axios-posts';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index'
// import { Route } from 'react-router-dom';
import Post from './Post/Post';
// import FullPost from './Fullpost/Fullpost';
import classes from './Posts.module.css';
import Spiner from '../UI/Spinner/Spinner';
import SearchInputForm from '../searchInputsForm/SearchInputForm';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'
class Posts extends Component {
    // state = {
    //     selectedPostId: null,
    //     loading : false

    // }

    componentDidMount () {
      
        this.props.oninitPosts();
      
    }
  
      postSelectedHandler = (id) => {
        this.props.history.push( '/posts/' + id );
     
        
    }
    PostsSearchHandler = (search)=>{
        this.props.onSearchPosts(search);
    }
    render () {
       
        let posts = <p style={{textAlign: 'center'}}>Something went wrong!</p>;
        if (!this.props.error) {
            posts = this.props.posts.map(post => {
                return <Post 
                    key={post.id} 
                    post={post}
                    clicked={() => this.postSelectedHandler(post.id)} />;
            });
        }
        if (this.props.loading){
            posts = <Spiner/>
        }
                return (
                    
                    <>
                     <SearchInputForm posts={this.props.posts} PostsSearchHandler={this.PostsSearchHandler}/>
                    <section className={classes.posts}>
                    {posts}
                    </section>
                      </>
                   
                  
             );
         
        };

  
}
    const mapStateToProps = state => {
        return {
            posts: state.posts.posts,
            error: state.posts.error,
            loading: state.posts.loading,
            found: state.posts.found
        };
    }
    
    const mapDispatchToProps = dispatch => {
        return {
           oninitPosts : ()=> dispatch(actions.initPosts()),
           onSearchPosts: (searchData) => dispatch(actions.searchPosts(searchData))
           
        }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Posts , axios ));