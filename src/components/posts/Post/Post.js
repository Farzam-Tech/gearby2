import React from 'react';
// import Image1 from '../../../assets/images/02.jpg'
import classes from './Post.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBookmark } from '@fortawesome/free-solid-svg-icons'; 
const post = (props) => (
    
   <div className={classes.post} onClick={props.clicked}>
       <figure className={classes.postimage}><img src={props.post.image} alt="pic1"/></figure>
       <div className={classes.postinfo}>
           <div className={classes.postinfoitems}>
         <h4>{props.post.name}</h4><FontAwesomeIcon icon={faBookmark} />
           </div>
           <div className={classes.postinfoitems}>
              <p>{props.post.field} , {props.post.time}</p>
           </div>
           <div className={classes.postinfoitems}>
            <p>{props.post.location[0].address}</p>
           </div>

       </div>
   </div>
           

);


export default post;