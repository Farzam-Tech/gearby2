import React, { Component } from 'react';
import axios from '../../../axios-posts'
import Spiner from '../../UI/Spinner/Spinner';
import classes from './Fullpost.module.css'
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';

class FullPost extends Component {

    componentDidMount () {
     
        this.props.onInitFullPost(this.props.match.params.id , this.props.loadedPost);
    }


    render () {
        let post = <p style={{ textAlign: 'center' }}>something went wrong!</p>;
        if ( this.props.match.params.id && !this.props.error) {
            post = <p style={{ textAlign: 'center' }}>Loading...!</p>;
            if (this.props.loading){
                post = <Spiner/>
            }
        }
        if ( this.props.loadedPost && !this.props.error ) {
          
            post =  (
                  
                <section className={classes.fullpost}>
                  <div className={classes.postdetails}>
                     <figure className={classes.postimage}><img src={ this.props.loadedPost.image} alt="pic"/></figure>

                  <div className={classes.highlights}>
                      <strong className={classes.items}>{this.props.loadedPost.name}</strong>
                      <p className={classes.items}><strong>Field : </strong>{this.props.loadedPost.field}</p>
                      <p className={classes.items}><strong>Operation Hours : </strong>{this.props.loadedPost.time}</p>
                      <p className={classes.items}><strong>Address : </strong>{this.props.loadedPost.location[0].address}</p>
                  </div>
                  
                 
                       <div className={classes.highlights}><strong>Highlights : </strong>{this.props.loadedPost.highlights}</div>
                     <div className={classes.reviews}>
                         <h4 className={classes.reviewsLabel}>REVIEWS:</h4>
                         <div className={classes.reviewsscreen}>
                             {this.props.loadedPost.reviews.map((post)=>{
                                 return <div className={classes.singlereview}><p style={{fontWeight: "bolder"}}>{post.name} :&nbsp;&nbsp; </p>  {post.review}</div>
                             })}
                             {/* <div className={classes.singlereview}></div>
                             <div className={classes.singlereview}></div>
                             <div className={classes.singlereview}></div> */}
                         </div>
                     </div>
                  </div>
                 
                </section>
                 
          

            );
        }
        return post;
      
    }
}
const mapStateToProps = state => {
    return {
        loadedPost: state.fullPostReducer.loadedPost,
        error: state.fullPostReducer.error,
        loading: state.fullPostReducer.loading
    };
}
const mapDispatchToProps = dispatch => {
    return {
       onInitFullPost : (id ,loadedPost)=> dispatch(actions.initFullPost(id ,loadedPost )),
      

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FullPost , axios);
