export const SET_POSTS = 'SET_POSTS';
export const FETCH_POSTS_FAILED = 'FETCH_POSTS_FAILED';
export const INIT_FULL_POST_SUCCESS = 'INIT_FULL_POST_SUCCESS';
export const INIT_FULL_POST_FAIL='INIT_FULL_POST_FAIL';
export const INIT_FULL_POST_START='INIT_FULL_POST_START';
export const POSTS_SEARCH_FAIL = 'POSTS_SEARCH_FAIL';
export const POSTS_SEARCH_SUCCESS = 'POSTS_SEARCH_SUCCESS';
export const POSTS_SEARCH_START = 'POSTS_SEARCH_START';
export const POSTS_FOUND='POSTS_FOUND';
export const POSTS_FILTER_BY_DETAIL='POSTS_FILTER_BY_DETAIL'