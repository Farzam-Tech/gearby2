import * as actionTypes from './actionTypes';
import axios from '../../axios-posts';


export const postsSearchSuccess = (posts) => {
    return{
        type: actionTypes.POSTS_SEARCH_SUCCESS,
        posts : posts
    }

};
export const postsSearchFail = (error) =>{
    return{
        type: actionTypes.POSTS_SEARCH_FAIL,
        error: error
    }
};
export const postsSearchStart = ()=>{
    return {
        type: actionTypes.POSTS_SEARCH_START
    };
};
export const postsFound = (posts)=>{
    return{
        type: actionTypes.POSTS_FOUND,
        posts : posts
    };
};
export const filterPostsByDetail = (posts , searchinfo)=>{
    return{
        type: actionTypes.POSTS_FILTER_BY_DETAIL,
        posts : posts, 
        searchinfo : searchinfo
    }
}

export const searchPosts = (search) => {
    
    return dispatch => {
        dispatch(postsSearchStart());
        axios.get('/posts.json')
        .then( response => {
              dispatch(postsSearchSuccess(response.data));
              if(search.Detail.value){
                 dispatch(filterPostsByDetail(response.data , search.Detail.value));
              }
            //   console.log(search.Detail.value);
              dispatch(postsFound(response.data));
              
                 
                }).catch(error => {
                  dispatch(postsSearchFail(error))
                });
              
}

    }