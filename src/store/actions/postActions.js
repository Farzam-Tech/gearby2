import * as actionTypes from './actionTypes';
import axios from '../../axios-posts'

export const setPosts = (posts) =>{
    return{
        type: actionTypes.SET_POSTS,
        posts: posts
    }
}; 
export const fetchPostsFailed = () =>{
   return {
       type: actionTypes.FETCH_POSTS_FAILED
   }
};
export const initPosts = () =>{
    return dispatch =>{
       
        axios.get('/posts.json')
            .then( response => {
                dispatch(setPosts(response.data));
                // const cities = response.data.cities;
                // this.setState({cities : cities , loading:false})
            //    console.log(response)
                    }).catch(error => {
                     dispatch(fetchPostsFailed())
                        // console.log(error);
                        // this.setState({error:true , loading:false});
                    });
    }
}