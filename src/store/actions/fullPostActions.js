import * as actionTypes from './actionTypes';
import axios from '../../axios-posts';

export const initFullPostStart = () =>{
    return {
        type: actionTypes.INIT_FULL_POST_START
    };
}
export const initFullPostSuccess = (loadedPost)=>{
    return{
        type: actionTypes.INIT_FULL_POST_SUCCESS,
        loadedPost: loadedPost
    }
}
export const initFullPostFail = (error)=>{
    return{
        type: actionTypes.INIT_FULL_POST_FAIL,
        error: error
    }
}
export const initFullPost = (id , loadedPost) =>{
    return dispatch=>{
        dispatch(initFullPostStart());
        if (id) {
            if ( !loadedPost || (loadedPost && loadedPost.id !== +id) ) {
            axios.get( '/posts/' + id + '.json')
            .then( response => {
                dispatch(initFullPostSuccess(response.data));
                
            } ).catch(error => {
              
                dispatch(initFullPostFail(error))
            });
            }
        }
      
    }
}