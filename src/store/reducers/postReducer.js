import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility'
const initialState = {
    posts: [],
    error: false ,
    loading: false, 
    found: false,
};
const setPosts = (state , action)=>{
    return updateObject(state , {posts: action.posts,
        error: false});
};
const fetchPostsFailed = ( state , action) =>{
    return updateObject(state , { error: true });
}
const postsSearchStart = (state, action) =>{
    return updateObject(state ,
         {  loading: true,
        error: false, found: false});
};
const postsFound = (state, action)=>{
    const res = state.posts;
    if(res.length > 0){
        return updateObject(state , { found: true});
    }
    
};
const postsSearchSuccess = (state, action)=>{
    
    return updateObject(state ,
         {posts: action.posts,
        loading: false});  
};
const postsSearchFailed = (state, action) =>{
    return updateObject(state , { error: true,
        loading: false});
};
const postsSearchByDetail = (state, action) =>{
    const posts = [...action.posts];
        let updatedPosts = posts.filter((res) => {
            console.log(res);
                      return(
                            res.name.toLowerCase().includes( action.searchinfo) ||
                            res.field.toLowerCase().includes( action.searchinfo ) ||
                            res.location[0].address.toLowerCase().includes( action.searchinfo)
                           ) 
                         
    })

return updateObject(state,
     {posts : updatedPosts}
     );
   
}
const reducer = (state = initialState, action) =>{
    switch(action.type){
        case actionTypes.SET_POSTS: return setPosts(state , action);
        case actionTypes.FETCH_POSTS_FAILED: return fetchPostsFailed(state , action);
        case actionTypes.POSTS_SEARCH_START: return postsSearchStart(state, action);
        case actionTypes.POSTS_FOUND: return postsFound(state, action);   
        case actionTypes.POSTS_SEARCH_SUCCESS:  return postsSearchSuccess(state, action);
        case actionTypes.POSTS_SEARCH_FAIL: return postsSearchFailed(state, action);
        case actionTypes.POSTS_FILTER_BY_DETAIL: return postsSearchByDetail(state,action);
                default:
                    return state;
    }
    };
    
    export default reducer;