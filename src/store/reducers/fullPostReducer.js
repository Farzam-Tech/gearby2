import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility'

const initialState = {

    loadedPost: null,
    loading: false, 
    found: false,
    error: false
 }
 const initFullPostStart = (state, action) => {
    return updateObject(state , {  loading: true, error: false});
    
}
const initFullPostSuccess = (state, action)=>{
    return updateObject(state , 
        {loadedPost: action.loadedPost,
        loading: false}); 
}
const initFullPostFail= (state, action) => {
    return updateObject(state , { error: true,
        loading: false});
}
 const fullPostReducer = (state = initialState, action) => {
    switch (action.type){
       
        case actionTypes.INIT_FULL_POST_START: return initFullPostStart(state,action);
        case actionTypes.INIT_FULL_POST_SUCCESS: return initFullPostSuccess(state, action);
        case actionTypes.INIT_FULL_POST_FAIL: return initFullPostFail(state,action);
        default: return state;
    }
    };
export default fullPostReducer;