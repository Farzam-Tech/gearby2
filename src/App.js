import React, { Component , Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './hoc/Layout/Layout';
// import post from './components/posts/Post/Post';
// import posts from './components/posts/Posts'
import FullPost from './components/posts/Fullpost/Fullpost';
import Posts from './components/posts/Posts';

const Posts = React.lazy(() => import('./components/posts/Posts'))
class App extends Component {
  render () {
    return (
      <div>
        <Layout>
          <Switch>
            {/* <Route path="/checkout" component={Checkout} />
            <Route path="/orders" component={Orders} /> */}
            {/* <Route path="/posts" exact component={posts} /> */}
            

        <Route path="/posts" exact render={(props) => (
              <Suspense fallback={<div>Loading...</div>}>
                <Posts {...props}/>
              </Suspense>
         )} />
            <Route path={'/posts/:id'} exact component={FullPost} />
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;

