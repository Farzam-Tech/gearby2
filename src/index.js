import React from 'react';
import ReactDOM from 'react-dom';
import { createStore , applyMiddleware , compose , combineReducers} from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import postsReducer from './store/reducers/postReducer';
import fullPostReducer from './store/reducers/fullPostReducer'
import axios from 'axios';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rootReducer = combineReducers({
    posts: postsReducer,
    fullPostReducer: fullPostReducer
});

const store = createStore(rootReducer , composeEnhancers (
    applyMiddleware(thunk)
))

axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN';
axios.defaults.headers.post['Content-Type'] = 'application/json';

axios.interceptors.request.use(request => {
    console.log(request);
    // Edit request config
    return request;
}, error => {
    console.log(error);
    return Promise.reject(error);
});

axios.interceptors.response.use(response => {
    console.log(response);
    // Edit request config
    return response;
}, error => {
    console.log(error);
    return Promise.reject(error);
});

const app = (
    <Provider store={store}>
    <BrowserRouter>
        <App />
    </BrowserRouter>

</Provider>
);

ReactDOM.render( app, document.getElementById( 'root' ) );
serviceWorker.unregister();
